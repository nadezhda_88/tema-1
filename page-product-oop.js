"use strict";
class AddReviewForm {
    constructor(form, inputName, errorNameScore, inputScore, errorNameElement, inputText) {
        this.form = form;
        this.inputName = inputName;
        this.errorNameElement = errorNameElement;
        this.inputScore = inputScore;
        this.errorNameScore = errorNameScore;
        this.inputText = inputText;
    }

    validate(event) {
        event.preventDefault()
       
        let name = inputName.value;
        let score = inputScore.value;
        let text = inputText.value;
       let errorName = "";
       let nameCorrect = name.length >= 2;
       let scoreCorrect = score > 0 && score < 6;

       if (nameCorrect && scoreCorrect){
        inputName.value = "";
        inputScore.value = "";
        inputText.value = ""; 

         localStorage.removeItem('name');
         localStorage.removeItem('grade');
         localStorage.removeItem('text');
         
     }  if (name.length === 0) {
        errorName = "Вы забыли указать Имя и Фамилию";
    }
        if (!nameCorrect) {
            errorName = "Имя не может быть короче 2-х символов";
        }
       
        errorNameElement.innerText = errorName;
        if (errorName) {
            errorNameElement.classList.add("visible");
            inputNameContainer.classList.add("input-name_error");
        } else {
            errorNameElement.classList.remove("visible");
            inputNameContainer.classList.remove("input-name_error");

        }
        if (errorName) return;

        let errorScore = "";

        if (!scoreCorrect) {
            errorScore = "Оценка должна быть от 1 до 5";
        }
        errorNameScore.innerText = errorScore;
        if (errorScore) {

            errorNameScore.classList.add("visible");
            inputScore.classList.add("input-grade_error");
        } else {
            errorNameScore.classList.remove("visible");
            inputScore.classList.remove("input-grade_error");
            return;
        }
    }
    clearerrorName() {
        errorNameElement.classList.remove('visible');
        inputName.classList.remove('input-name_error');
        errorNameElement.innerHTML = '';
        errorNameScore.classList.remove('visible');
        inputScore.classList.remove('input-grade_error');
       errorNameScore.innerHTML = '';
    }
    clearerrorScore() {
        errorNameScore.classList.remove("visible");
    }

    changeName() {
        localStorage.setItem('name', inputName.value);
    }

    changeGrade() {
        localStorage.setItem('grade', inputScore.value);
    }
    changetext() {
        localStorage.setItem('text', inputText.value);
    }
    }

let form = document.querySelector(".form");

let name = form.querySelector(".add-review__content");
let grade = form.querySelector(".add-review__content");

let inputNameContainer = form.querySelector(".input-name");
let inputScoreContainer = form.querySelector(".input-grade");

let inputName = inputNameContainer.querySelector(".add-review__content");
let errorNameElement = inputNameContainer.querySelector(".error");

let inputScore = inputScoreContainer.querySelector(".add-review__content");
let errorNameScore = inputScoreContainer.querySelector(".error");

let inputText = form.querySelector(".add-review__text")


let add_listener_form = new AddReviewForm(form, inputName, errorNameScore, inputScore, errorNameElement, inputText);

form.addEventListener('submit', add_listener_form.validate);
inputName.addEventListener('focus', add_listener_form.clearerrorName);
inputScore.addEventListener('focus', add_listener_form.clearerrorScore);
inputName.addEventListener('input', add_listener_form.changeName);
inputScore.addEventListener('input', add_listener_form.changeGrade);
inputText.addEventListener('input', add_listener_form.changetext);
inputName.value = localStorage.getItem('name');
inputScore.value = localStorage.getItem('grade');
inputText.value = localStorage.getItem('text');
