"use strict";

let form = document.querySelector(".form");


let name = form.querySelector(".add-review__content");
let grade = form.querySelector(".add-review__content");

let inputNameContainer = form.querySelector(".input-name");
let inputScoreContainer = form.querySelector(".input-grade");

let inputName = inputNameContainer.querySelector(".add-review__content");
let errorNameElement = inputNameContainer.querySelector(".error");

let inputScore = inputScoreContainer.querySelector(".add-review__content");
let errorNameScore = inputScoreContainer.querySelector(".error");

let inputText = form.querySelector(".add-review__text")

inputName.addEventListener('input', changeName);
inputScore.addEventListener('input', changeGrade);
inputText.addEventListener('input', changeText);
inputName.value = localStorage.getItem('name');
inputScore.value = localStorage.getItem('grade');
inputText.value = localStorage.getItem('text');


function handleSubmit(event) {
    event.preventDefault();

   
    let name = inputName.value;
    let score = inputScore.value;
    let text = inputText.value;
    let errorName = "";
    let errorScore = "";
    let nameCorrect = name.length >= 2;
    let scoreCorrect = score > 0 && score < 6;

    if (nameCorrect && scoreCorrect){
        inputName.value = "";
        inputScore.value = "";
        inputText.value = "";

         localStorage.removeItem('name');
         localStorage.removeItem('grade');
         localStorage.removeItem('text');
         
     }

    if (!nameCorrect) {
        errorName = "Имя не может быть короче 2-х символов";
    }
    if (name.length === 0) {
        errorName = "Вы забыли указать Имя и Фамилию";
    }
    errorNameElement.innerText = errorName;
    if (errorName) {
        errorNameElement.classList.add("visible");
        inputNameContainer.classList.add("input-name_error");
    } else {
        errorNameElement.classList.remove("visible");
        inputNameContainer.classList.remove("input-name_error");

    }
    if (errorName) return;
     
    
    if (!scoreCorrect) {
        errorScore = "Оценка должна быть от 1 до 5";
    }
    errorNameScore.innerText = errorScore;
    if (errorScore) {

        errorNameScore.classList.add("visible");
        inputScore.classList.add("input-grade_error");
    } else {
        errorNameScore.classList.remove("visible");
        inputScore.classList.remove("input-grade_error");
        return;
    }
}
inputName.addEventListener('input', function () {
    errorNameElement.classList.remove("visible");
    
})
inputScore.addEventListener('input', function () {
    errorNameScore.classList.remove("visible");
   
})

function changeName() {
    localStorage.setItem('name', inputName.value);
}

function changeGrade() {
    localStorage.setItem('grade', inputScore.value);
}

function changeText() {
    localStorage.setItem('text', inputText.value);
}
form.addEventListener("submit", handleSubmit);



let header = document.querySelector(".purchases");
let CartNumber = document.querySelector(".purchases__basket-number");
let choiseButton = document.querySelector(".button-basket");
let message = document.querySelector(".message-cart_hidden");


choiseButton.value = +localStorage.getItem("productNumber");
choiseButton.addEventListener("click", changeCart);


console.log(+choiseButton.value);



function changeCart() {
 if (+choiseButton.value === 0) {
    choiseButton.value += 1;
    console.log(+choiseButton.value);
    
    choiseButton.classList.add("button-basket_choise");
    CartNumber.classList.add("purchases__basket-number_visit");
    choiseButton.innerHTML = "Товар уже в корзине";
    localStorage.setItem("productNumber", +choiseButton.value);
  } else {
    choiseButton.value -= 1;
    console.log(+choiseButton.value);
    choiseButton.classList.remove("button-basket_choise");
    CartNumber.classList.remove("purchases__basket-number_visit");
    message.classList.remove("message-cart");
    choiseButton.innerHTML = "Добавить в корзину";
    localStorage.removeItem("productNumber", +choiseButton.value);
    return;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  
  if (+choiseButton.value === 0) {
    choiseButton.classList.remove("button-basket_choise");
    CartNumber.classList.remove("purchases__basket-number_visit");
    choiseButton.innerHTML = "Добавить в корзину";
  } else {
    choiseButton.classList.add("button-basket_choise");
    CartNumber.classList.add("purchases__basket-number_visit");
    choiseButton.innerHTML = "Товар уже в корзине";
    return;
  }
});


