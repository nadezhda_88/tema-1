"use strict";
//Упражнение 1
/**
 * Возвращение значения
 * @returns {isEmpty} - Проверяет наличие свойст объекта и возвращает значение согласно заданных условий
  */
let obj = {};
function isEmpty(obj) {
    for (let key in obj) {
        return true;
    }
    return false;
}
console.log(isEmpty(obj));


//Упражнение 3

let salary = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};
let summ = 0;
/**
 * Подсчет зарплаты
 * @returns {raiseSalary} - Повышает  зарплаты на задаваемый процент и  возвращает объект с новыми зарплатами
 */
(function raiseSalary() {
    let value = +prompt("Введите процент повышения зарплаты");
    for (let key in salary) {
        salary[key] = salary[key] + salary[key] * value / 100;
        console.log(salary[key])
        summ += salary[key];

    }
    return console.log(Math.floor(summ));
})()
