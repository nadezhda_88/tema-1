"use strict";

//Упражнение 1
let arr1 = [1, 2, 10, 5];
let arr2 = ["a", {}, 3, 3, -2, "?", 9, "<>", -12];
let sum = 0;
function getSumm() {
    for (let i = 0; i < arr2.length; i++) {
        if (isNaN(arr2[i])) continue;
        sum += arr2[i];
    }
    return sum;
}
alert(getSumm());

//Упражнение 3

let cart = [4884];
function addToCart(productId) {
    let hasInCart = cart.includes(productId);

    if (hasInCart) return;

    cart.push(productId);
}

function removeFromCart(productId) {
    cart = cart.filter(function (id) {
        return id !== productId
    });
}

//Добавили товар

addToCart(3456);
addToCart(3456);
console.log(cart);

//Удалили товар

removeFromCart(4884);
console.log(cart);



