import React from 'react';
import'./PageProduct.css';

function PageProduct() {
    return (
    <div className='wrapper'>
    <div className='content'>
      <header>
        <div className='side-header'>
          <div className='logo'>
            <img src="./image/favicon.svg" alt="Лого в шапке"/>
            <h1><span className="text-color">Мой</span>Маркет</h1>
          </div>
          <div className="purchases">
            <div className='purchases__heart'><img src="./image/heart.svg"/></div>
            <div className='purchases__heart-number'>1</div>
            <div className='purchases__basket'><img src="./image/Vector.svg"/></div>
            <div className='purchases__basket-number'>1</div>
          </div>
        </div>
      </header>
      <nav className='breadcrumbs'>
        <a href="#">Электроника</a>
        <span>"&gt;"</span>
        <a href="#">Смартфоны и гаджеты</a>
        <span>"&gt;"</span>
        <a href="#">Мобильные телефоны</a>
        <span>"&gt;"</span>
        <a href="#">Apple</a>
      </nav>
      <main>
            <section className='smartphone'>
          <h2 className='smartphone__name'>Smartfon Apple iPhone 13, blue</h2>
          <div className='smartphone__photos'>
            <img className='smartphone__photo1' src="./image/image-1.png" alt="Вид спереди"/>
            <img className='smartphone__photo2' src="./image/image-2.png" alt="Главный экран"/>
            <img className='smartphone__photo3' src="./image/image-3.png" alt="Вид сбоку"/>
            <img className='smartphone__photo4' src="./image/image-4.png" alt="Камера"/>
            <img className='smartphone__photo5' src="./image/image-5.png" alt="Вид сзади"/>
          </div>
        </section>
        <section className='characteristics'>
          <div className='characteristics-left'>
            <div className="product">
              <h3 className="product__name">Цвет товара: Синий</h3>
              <div className="product__list">
                <div className="product__pictures">
                  <label><input  type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-1 2.png" alt="Красный телефон"
                        height="60"/></div>
                  </label>
                  <label><input  type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-2 1.png" alt="Зеленый телефон"
                        height="60"/></div>
                  </label>
                  <label><input type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-3 1.png" alt="Розовый телефон"
                        height="60"/></div>
                  </label>
                  <label><input type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-4 1.png" alt="Синий телефон" height="60"/>
                    </div>
                  </label>
                  <label><input type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-5 1.png" alt="Белый телефон" height="60"/>
                    </div>
                  </label>
                  <label><input type="radio" name="product-color"/>
                    <div className="product__button-color"><img src="./image/color-6 1.png" alt="Черный телефон"
                        height="60"/></div>
                  </label>
                </div>
              </div>
            </div>
            <div className="product-memory">
              <h3 className="product-memory__name">Конфигурация памяти: 128 ГБ</h3>
              <div className="product-memory__buttons">
                <label><input type="radio" name="product-memory"/>
                  <div className="product-memory__button ">128ГБ</div>
                </label>
                <label><input type="radio" name="product-memory"/>
                  <div className="product-memory__button">256ГБ</div>
                </label>
                <label><input type="radio" name="product-memory"/>
                  <div className="product-memory__button">512ГБ</div>
                </label>
              </div>
            </div>
            <div className="product-characteristics">
              <h3 className="product-characteristics__name">Характеристики товара</h3>
              <div className="product-characteristics__list">
                <div className="product-characteristics__text">Экран:<span>6.1</span></div>
                <div className="product-characteristics__text">Встроенная память:<span>128 ГБ</span></div>
                <div className="product-characteristics__text">Операционная система: <span>iOS 15</span></div>
                <div className="product-characteristics__text">Беспроводные интерфейсы: <span>NFC, Bluetooth, Wi-Fi</span>
                </div>
                <div className="product-characteristics__text">Процессор:<a href="https://ru.wikipedia.org/wiki/Apple_A15"
                    target="_blank">
                    <span>Apple A15 Bionic</span></a></div>
                <div className="product-characteristics__text">Вес:<span>173 г</span></div>
              </div>
            </div>
            <div className="product-description">
              <h3 className="product-description__name">Описание</h3>
              <p >Наша самая совершенная система двух камер.<br/>
                Особый взгляд на прочность дисплея.<br/>
                Чип, с которым всё супербыстро.<br/>
                Аккумулятор держится заметно дольше. <br/>
                <i>iPhone 13 - сильный мира всего.</i>
              </p>
              <p>Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов. Благодаря
                этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной
                камеры.
                Кроме того, мы освободили место для системы оптической стабилизацииизображения сдвигом матрицы. И
                повысили скорость работы матрицы насверхширокоугольной камере.</p>
              <p>Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая широкоугольная
                камера улавливает на 47% больше света для
                более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы обеспечит чёткие
                кадры даже в неустойчивом положении.</p>
              <p>Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости.
                Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая
                красивый эффект размытиявокруг него.
                Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который
                появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино. </p>
            </div>
            <section className="table-comparison">
              <h3 className="table-comparison__name">Сравнение моделей</h3>
              <table className="table-comparison__table">
                <tbody>
                  <tr>
                    <th>Модель</th>
                    <th>Вес</th>
                    <th>Высота</th>
                    <th>Ширина</th>
                    <th>Толщина</th>
                    <th>Чип</th>
                    <th>Объём памяти</th>
                    <th>Аккумулятор</th>
                  </tr>
                  <tr>
                    <td>Iphone11</td>
                    <td>194 грамма</td>
                    <td>150.9 мм</td>
                    <td>75.7 мм</td>
                    <td>8.3 мм</td>
                    <td>A13 Bionicchip</td>
                    <td>до 128 Гб</td>
                    <td>До 17 часов</td>
                  </tr>
                  <tr>
                    <td>Iphone12</td>
                    <td>164 грамма</td>
                    <td>146.7 мм</td>
                    <td>71.5 мм</td>
                    <td>7.4 мм</td>
                    <td>A14 Bionicchip</td>
                    <td>до 256 Гб</td>
                    <td>До 19 часов</td>
                  </tr>
                  <tr>
                    <td>Iphone13</td>
                    <td>174 грамма</td>
                    <td>146.7 мм</td>
                    <td>71.5 мм</td>
                    <td>7.65 мм</td>
                    <td>A15 Bionicchip</td>
                    <td>до 512 Гб</td>
                    <td>До 19 часов</td>
                  </tr>
                </tbody>
              </table>
            </section>
          </div>
          <aside>
            <div className="important">
              <div className="price">
                <div className="old-price">
                  <div className="old-price__price">75990₽</div>
                  <div className="old-price__discount">-8%</div>
                  <svg widt="28" height="22" viewBox="0 0 28 22" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                      d="M2.78502 2.57269C5.17872 0.274736 9.04661 0.274736 11.4403 2.57269L14.0001 5.03017L16.56 2.57269C18.9537 0.274736 22.8216 0.274736 25.2154 2.57269C27.609 4.87064 27.609 8.5838 25.2154 10.8818L14.0001 21.6483L2.78502 10.8818C0.391321 8.5838 0.391321 4.87064 2.78502 2.57269ZM9.67253 4.26974C8.25515 2.90905 5.97018 2.90905 4.55278 4.26974C3.1354 5.63043 3.1354 7.82401 4.55278 9.18476L14.0001 18.2542L23.4476 9.18476C24.865 7.82401 24.865 5.63043 23.4476 4.26974C22.0302 2.90905 19.7452 2.90905 18.3279 4.26974L14.0001 8.42432L9.67253 4.26974Z" />
                  </svg>
                </div>
                <div className="actual-price">67 990₽</div>
              </div>
              <div className="delivery">
                <div className="delivery__text">
                  <div>Самовывоз в четверг, 1 сентября —<span>бесплатно</span></div>
                  <div>Курьером в четверг, 1 сентября —<span>бесплатно</span></div>
                </div>
              </div>
              <button className="button-basket ">Добавить в корзину</button>
            </div>
           <div className="advertising">
           <p class="advertising__name">Реклама</p>
              <iframe src="./abs.html" width="400" height="300"></iframe>
              <iframe src="./abs.html" width="400" height="300"></iframe>
            </div>
          </aside>
          <div className="message-cart_hidden"> Товар добавлен в корзину</div>
        </section>
        <section className='reviews-section'>
          <div className="reviews-section__header">
            <div className="reviews-section__wrapper">
              <h3 className="reviews-section__title">Отзывы</h3>
              <span className="reviews-section__count"> 452</span>
            </div>
          </div>
          <div className="reviews-section__list">
            <div className="review">
              <div className="review__images"><img class="review__image" src="./image/Фотография автора.png " alt="Марк Г."/>
              </div>
              <div className="review__description">
                <h3 className="review__name"> Марк Г.</h3>
                <div className="review__rating">
                  <img class="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img class="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img class="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img class="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img class="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                </div>
                <div className="review__parametrs">
                  <div className="review__parametr">
                    <b>Опыт использования:</b> менее месяца
                  </div>
                  <div className="review__parametr">
                    <div><b>Достоинства:</b></div>
                    <div>это мой первый айфон после после огромного количества телефонов на андроиде. всёплавно,
                      чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.</div>
                  </div>
                  <div className="review__parametr">
                    <div><b> Недостатки:</b></div>
                    <div>к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь
                      а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество
                      фотографий исходное.</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="separator"></div>
            <div className="review">
              <div className="review__images"><img class="review__image" src="./image/Фотография автора (1).png "
                  alt="Валерий Коваленко"/></div>
              <div className="review__description">
                <h3 className="review__name">Валерий Коваленко</h3>
                <div className="review__rating">
                  <img className="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img className="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img className="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img className="review__rating-star" src="./image/Звезда.png" alt="звезда"/>
                  <img className="review__rating-star" src="./image/Звезда (1).png" alt="звезда"/>
                </div>
                <div className="review__parametrs">
                  <div className="review__parametr"><b>Опыт использования:</b> менее месяца</div>
                  <div className="review__parametr">
                    <div><b>Достоинства:</b></div>
                    <div>OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</div>
                  </div>
                  <div className="review__parametr">
                    <div><b> Недостатки:</b></div>
                    <div>Плохая ремонтопригодность</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="form">
          <form class="add-review">
            <h3 class="add-review__title">Добавить свой отзыв</h3>
            <div class="add-review__fields">
              <div class="add-review__first-line">
                <div class="input-name">
                  <input class="add-review__content" type="text" autocomplete="off" placeholder="Имя и фамилия"
                    size="50" name="name"/>
                  <div class="error"></div>
                </div>
                <div class="input-grade">
                  <input class="add-review__content" type="number"  autocomplete="off" placeholder="Оценка"
                    name="grade"/>
                  <div class="error"></div>
                </div>
              </div>
              <textarea class="add-review__text" placeholder="Текст отзыва" name="text"></textarea>
            </div>
            <input class="add-review__button" type="submit" value="Отправить отзыв" name="form-btn"/>
          </form>
        </section>
        </main>
         <footer >
        <div className="footer-content">
          <div className="footer-content__copyright">
            <p><b> &copy; ООО «<span className="text-color"> Мой</span>Маркет», 2018-2022.</b></p>
            <p>Для уточнения информации звоните по номеру<a href="tel:+79000000000">+79000000000</a>,</p>
            <p> а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com">
                partner@mymarket.com</a></p>
          </div>
          <div className="footer-content__up"><a href="#">Наверх</a></div>
        </div>
      </footer>
      </div>
      </div>
    );  
}
export default PageProduct;
