"use strict";
//Упражнение 1
for (let a = 2; a <= 20; a = a + 2) {
   console.log(a);
}


//Упражнение 2

let summ = 0; 
for (let i = 0; i < 3; i++) { 
   let value = +prompt("Введите число", ""); 
   summ += value;
   if (isNaN(value)) {
      alert('Ошибка, выввели не число'); 
      break;
   }
}
if (!isNaN(summ)) { 
   alert("Сумма: " + summ);
}

//Упражнение 3
let a = 0
function getNameOfMonth() {
   let month = ('январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь')
   month = month.split(' ')
   console.log(month[a])
}

for (a; a < 12; a++) {
   if (a === 9) continue;
   getNameOfMonth()
}
// Упражнение 4
//IIFE (Immediately Invoked Function Expression) - немеделенно вызываемая функция. конструкция позволяющая вызвать функцию сразу после ее определения

(function () {
   let month = ('январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь');
   month = month.split(' ');
   console.log(month);
})();